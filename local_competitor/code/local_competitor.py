#!/usr/bin/env/ python3.7

import os
import sys
import math
import requests
import json
import xlrd
import time
import googlemaps
import openpyxl
import psycopg2
from psycopg2.extras import Json, DictCursor


# main function will get lat/long from spreadsheet, then call api, then update spreadsheet
def main():
    data = getExcelData()
    latLong = data[0]
    brands = data[1]
    radius = 8050
    dealer_dict = {}
    for dealer in latLong:
        latitude = latLong[dealer][0]
        longitude = latLong[dealer][1]
        address = latLong[dealer][2]
        jsonResponse = apiCall(dealer, latitude, longitude, address, radius, brands)
        total = jsonResponse[0]
        branded = jsonResponse[1]
        unbranded = jsonResponse[2]
        print("total = " + str(total))
        print("branded = " + str(branded))
        print("unbranded = " + str(unbranded))
        dealer_dict[dealer] = [total, branded, unbranded]
        # print(dealer_dict)
    print()
    print(dealer_dict)
    updateSite(dealer_dict)


# function to populate nested dict with lat/long of each site
def getExcelData():
    # relative filepath for sharepoint
    wb = "exxon_sites_2019_fraud_test.xlsx"
    siteData = xlrd.open_workbook(wb)
    sites_sheet = siteData.sheet_by_index(0)
    sites_dict = {}
    for i in range(1, sites_sheet.nrows):
        cur_row = sites_sheet.row(i)
        dealer = int(cur_row[0].value)
        address = cur_row[1].value
        lt = cur_row[2].value
        lg = cur_row[3].value
        sites = {dealer: [lt, lg, address]}
        sites_dict.update(sites)
    brands_sheet = siteData.sheet_by_index(1)
    brands = []
    for i in range(0, brands_sheet.nrows):
        cur_row = brands_sheet.row(i)
        brandName = cur_row[0].value
        if isinstance(brandName, float) is True:
            brandName = int(brandName)
        brandName = str(brandName)
        brandName = brandName.lower()
        brands.append(brandName)
    print("\n")
    print(sites_dict)
    print("\n")
    print(brands)
    print("\n")
    return sites_dict, brands


# old function that completes the api call for each record
# def apiCall(latitude, longitude, rad, token, next_page_t):
    # def apiCall(latitude, longitude, radius, query) - if we want to expand this code to other verticals besides petro
    # url = "https://maps.googleapis.com/maps/api/place/textsearch/json?query="
    # query = "gas+station"
    # lt = latitude
    # lg = longitude
    # location = "&location=" + str(lt) + "," + str(lg)
    # r = str(rad)
    # r = "&radius=" + r
    # key = "&key=" + "AIzaSyB_4BNjff6kRbTJ5q680mO0w0eeu3jYqqE"
    # print("\nthis is the apicall function token = " + str(token))
    # print("\nthis is the npt used = " + next_page_t)
    # if token == 0:
    #     print("token 0 request")
    #     response = requests.get(url + query + location + r + key)
    #     response = response.json()
    #     # print(json.dumps(response, indent=4, sort_keys=True))
    # else:
    #     print("pageToken request")
    #     npt = "&pageToken=" + next_page_t
    #     response = requests.get(url + query + location + r + key + npt)
    #     # response = requests.get(url + query + key + npt)
    #     response = response.json()
    #     # print(json.dumps(response, indent=4, sort_keys=True))


# api call function that calls the places api and uses the googlemaps package to iterate the next_page_token when it exists
def apiCall(dealer, latitude, longitude, address, rad, brands):
    # api call function that utilizes googlemaps api python package #####
    params = {'query': 'gas+station', 'location': (latitude, longitude), 'radius': rad}
    # url = "https://maps.googleapis.com/maps/api/place/textsearch/json"
    apikey = "AIzaSyB_4BNjff6kRbTJ5q680mO0w0eeu3jYqqE"
    gmaps = googlemaps.Client(key=apikey)
    response = gmaps.places(**params)
    npt = response.get("next_page_token")
    analysis = jsonAnalyze(dealer, response, brands, address, latitude, longitude)
    total = analysis[0]
    branded = analysis[1]
    unbranded = analysis[2]
    distance = analysis[3]

    while npt and distance <= 10:
        time.sleep(2)
        params['page_token'] = npt
        responseNPT = gmaps.places(**params)
        analysis = jsonAnalyze(dealer, responseNPT, brands, address, latitude, longitude)
        npt = responseNPT.get("next_page_token")
        print(npt)
        total += analysis[0]
        branded += analysis[1]
        unbranded += analysis[2]
        distance = analysis[3]

    return total, branded, unbranded


# function to count total sites, total branded sites, total unbranded sites
# total sites = total branded sites + total unbranded sites
def jsonAnalyze(dealer, json_data, brands, address, latitude, longitude):
    # how to access list of dictionaries, where the list is the value of a dictionary key?
    results = json_data.get("results")
    total = 0
    branded = 0
    unbranded = 0
    print(json.dumps(results, indent=4, sort_keys=True))
    updateDB(dealer, results)
    exit()
    for record in results:
        print(record)
        exit()
        name = record.get("name")
        json_address = record.get("formatted_address")
        geometry = record.get("geometry")
        geo_loc = geometry.get("location")
        geo_lat = geo_loc.get("lat")
        geo_long = geo_loc.get("lng")
        distance = calcCoordDist(latitude, longitude, geo_lat, geo_long)
        print(name)
        print(json_address)
        print(distance)
        # conditional to check physical radius
        if distance < 0.01:
            continue
        elif distance > 5 and distance <= 10:
            continue
        elif distance > 10:
            break
        name = name.lower()
        json_address = json_address.lower()
        address = address.lower()
        # conditional to prevent counting the site we are checking against itself
        if address == json_address:
            continue
        total += 1
        for brand in brands:
            if brand in name:
                branded += 1
        unbranded = total - branded

    return total, branded, unbranded, distance


# function to update the workbook with the total sites, branded sites, unbranded sites
def updateSite(dealer_dict):
    file = "exxon_sites_2019_fraud_test.xlsx"
    fraud_col = 5
    test_total_col = 6
    test_branded_col = 7
    test_unbranded_col = 8
    all_site_conv_col = 9
    majority_site_conv_col = 10
    half_site_conv_col = 11
    # file = "exxon_sites_2019_fraud_all_data.xlsx"
    # fraud_col = 24
    # total_col = 25
    # branded_col = 26
    # unbranded_col = 27
    wb = openpyxl.load_workbook(file)  # load the entire workbook
    ws = wb.active  # access the active (default to first) sheet
    row_count = ws.max_row  # find the number of rows
    for dealer in dealer_dict:  # iterate through the dealer dict by dealerID key
        total = dealer_dict[dealer][0]
        branded = dealer_dict[dealer][1]
        unbranded = dealer_dict[dealer][2]
        for dealerIDs in ws.iter_cols(min_row=2, max_col=1, max_row=row_count):  # iterate through the dealerIDs from sheet to compare to dealer from api call
            for dealerID in dealerIDs:
                cellVal = dealerID.internal_value
                if cellVal == dealer:
                    fraud = ws.cell(row=dealerID.row, column=fraud_col).value
                    if fraud is None:
                        fraud = 0
                    conv_dollars = calcFraudConv(fraud, total)
                    ws.cell(row=dealerID.row, column=test_total_col).value = total
                    ws.cell(row=dealerID.row, column=test_branded_col).value = branded
                    ws.cell(row=dealerID.row, column=test_unbranded_col).value = unbranded
                    ws.cell(row=dealerID.row, column=all_site_conv_col).value = conv_dollars[0]
                    ws.cell(row=dealerID.row, column=majority_site_conv_col).value = conv_dollars[1]
                    ws.cell(row=dealerID.row, column=half_site_conv_col).value = conv_dollars[2]
    wb.save(file)


# function which calculates the distance in mi between two decimal degree coordinates
def calcCoordDist(lat1, long1, lat2, long2):
    R = 3961.0
    # R = 6373.0  # radius of earth in km
    lat1 = float(lat1)
    long1 = float(long1)
    lat2 = float(lat2)
    long2 = float(long2)

    lat1 = math.radians(lat1)
    long1 = math.radians(long1)
    lat2 = math.radians(lat2)
    long2 = math.radians(long2)

    deltaLat = lat2 - lat1
    deltaLong = long2 - long1
    a = math.sin(deltaLat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(deltaLong / 2) ** 2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = R * c
    return d


# function to calculate the expected fraud amount for one site after a % of local competitors convert
def calcFraudConv(fraud, total_sites):
    tot_fraud = fraud * total_sites + fraud
    maj = tot_fraud / (total_sites - math.ceil(total_sites * 0.75) + 1)  # 75% of local competitors convert
    half = tot_fraud / (total_sites - math.ceil(total_sites * 0.5) + 1)  # 50% of local competitors convert
    return tot_fraud, maj, half


# function to add the json to the db
def updateDB(dealer, results_json):
    # dealer = str(dealer)
    pgDBHost = "localhost"
    pgDB = "em_json"
    pgDBUser = "postgres"
    pgDBPassword = "C@pr@Postgres"
    # pgTable = "dealers"
    pgDBPort = 5432

    conn = psycopg2.connect(host=pgDBHost, database=pgDB, user=pgDBUser, password=pgDBPassword, port=pgDBPort)
    cur = conn.cursor(cursor_factory=DictCursor)

    # sql_id = ("INSERT INTO dealers (id) VALUES (%s);")
    sql_json = ("INSERT INTO dealers (info) VALUES (%s);")

    print("PostgreSQL databse version: ")
    cur.execute("select version()")
    db_version = cur.fetchone()
    print(db_version)

    # cur.executemany(sql_json, results_json)
    # conn.commit()

    record = {"customer": "em1", "address": "123 abc blvd"}
    print(type(record))
    record = json.dumps(record)
    cur.execute(sql_json, (1, [record]))

    # for record in results_json:
    #     print()
    #     print()
    #     print(dealer)
    #     print(type(dealer))
    #     print()
    #     print(record)
    #     print()
    #     # cur.execute(sql_id, [dealer])
    #     cur.execute(sql_json, [record])
    #     # cur.execute(sql_id, (dealer, Json(record)))
    #     conn.commit()

    cur.close()
    conn.close()


if __name__ == "__main__":
    main()
