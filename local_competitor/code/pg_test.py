#!/usr/bin/env/ python3.7

import os
import sys
import math
import psycopg2
from config import config


def main():
    pgDBHost = "localhost"
    pgDB = "dvdrental"
    pgDBUser = "postgres"
    pgDBPassword = "C@pr@Postgres"
    pgDBPort = 5432

    # need commands to create a table for the dealerID and its json data

    conn = psycopg2.connect(host=pgDBHost, database=pgDB, user=pgDBUser, password=pgDBPassword, port=pgDBPort)
    cur = conn.cursor()
    print("PostgreSQL databse version: ")
    cur.execute("select version()")
    db_version = cur.fetchone()
    print(db_version)
    cur.close()
    conn.close()


def create_tables():
    commands = (
        """
        create table dealer (
            dealerID VARCHAR PRIMARY KEY
            address VARCHAR
            distance 
        )
        """
        )

if __name__ == "__main__":
    main()
